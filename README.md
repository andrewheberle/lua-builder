# Lua Builder

Builds Lua 5.3 from source and packages as a RPM for Centos 7 along with other Lua libraries

## Versions

### Lua

* 5.3.5-4 - [![pipeline status](https://gitlab.com/andrewheberle/lua-builder/badges/5.3.5-4/pipeline.svg)](https://gitlab.com/andrewheberle/lua-builder/commits/5.3.5-4) - [Download](https://gitlab.com/andrewheberle/lua-builder/-/jobs/219650611/artifacts/raw/rpmbuild/RPMS/x86_64/lua53-5.3.5-4.el7.x86_64.rpm)
* 5.3.5-3 - [![pipeline status](https://gitlab.com/andrewheberle/lua-builder/badges/5.3.5-3/pipeline.svg)](https://gitlab.com/andrewheberle/lua-builder/commits/5.3.5-3) - [Download](https://gitlab.com/andrewheberle/lua-builder/-/jobs/205739287/artifacts/raw/rpmbuild/RPMS/x86_64/lua53-5.3.5-3.el7.x86_64.rpm)
* 5.3.5-2 - [![pipeline status](https://gitlab.com/andrewheberle/lua-builder/badges/5.3.5-2/pipeline.svg)](https://gitlab.com/andrewheberle/lua-builder/commits/5.3.5-2) - [Download](https://gitlab.com/andrewheberle/lua-builder/-/jobs/196514972/artifacts/raw/rpmbuild/RPMS/x86_64/lua53-5.3.5-2.el7.x86_64.rpm)
* 5.3.5-1 - [![pipeline status](https://gitlab.com/andrewheberle/lua-builder/badges/5.3.5-1/pipeline.svg)](https://gitlab.com/andrewheberle/lua-builder/commits/5.3.5-1) - [Download](https://gitlab.com/andrewheberle/lua-builder/-/jobs/196514972/artifacts/raw/rpmbuild/RPMS/x86_64/lua53-5.3.5-1.el7.x86_64.rpm)

### Luasocket

* v3.0.rc1.211.gc89a931-1 - [![pipeline status](https://gitlab.com/andrewheberle/lua-builder/badges/5.3.5-4/pipeline.svg)](https://gitlab.com/andrewheberle/lua-builder/commits/5.3.5-4) - [Download](https://gitlab.com/andrewheberle/lua-builder/-/jobs/219650611/artifacts/raw/rpmbuild/RPMS/x86_64/lua53-luasocket-v3.0.rc1.211.gc89a931-1.el7.x86_64.rpm)

### CJSON

* 2.1.0-1 - [![pipeline status](https://gitlab.com/andrewheberle/lua-builder/badges/5.3.5-4/pipeline.svg)](https://gitlab.com/andrewheberle/lua-builder/commits/5.3.5-4) - [Download](https://gitlab.com/andrewheberle/lua-builder/-/jobs/219650611/artifacts/raw/rpmbuild/RPMS/x86_64/lua53-cjson-2.1.0-1.el7.x86_64.rpm)
