Name: lua53
Version: 5.3.5
Release: 4%{?dist}
Source0: https://www.lua.org/ftp/lua-%{version}.tar.gz
Summary: Powerful light-weight programming language
URL: https://www.lua.org/
License: MIT
Group: Development/Languages
Requires: readline
BuildRequires: readline-devel

%description
Lua is a powerful light-weight programming language designed for
extending applications. Lua is also frequently used as a
general-purpose, stand-alone language. Lua is free software.
Lua combines simple procedural syntax with powerful data description
constructs based on associative arrays and extensible semantics. Lua
is dynamically typed, interpreted from bytecodes, and has automatic
memory management with garbage collection, making it ideal for
configuration, scripting, and rapid prototyping.

%prep
%setup -q -n lua-%{version}

%build
make linux

%install
make install INSTALL_TOP=%{buildroot}/opt/lua53

%files
/opt/lua53
