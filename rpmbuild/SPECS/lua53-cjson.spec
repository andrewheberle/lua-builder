Name: lua53-cjson
Version: 2.1.0
Release: 1%{?dist}
Source0: https://www.kyne.com.au/~mark/software/download/lua-cjson-%{version}.tar.gz
Summary: Lua JSON library
URL: https://www.kyne.com.au/~mark/software/lua-cjson.php
License: MIT
Group: Development/Languages
Requires: lua53
BuildRequires: lua53

%description
Lua CJSON provides JSON support for Lua.

The Lua CJSON module provides JSON support for Lua. It features:
- Fast, standards compliant encoding/parsing routines
- Full support for JSON with UTF-8, including decoding surrogate pairs
- Optional run-time support for common exceptions to the JSON specification
  (infinity, NaN,..)
- No dependencies on other libraries

%prep
%setup -q -n lua-cjson-%{version}

%build
make PREFIX=/opt/lua53 LUA_VERSION=5.3

%install
make install PREFIX=/opt/lua53 LUA_VERSION=5.3 DESTDIR=%{buildroot}

%files
/opt/lua53
