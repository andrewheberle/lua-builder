Name: lua53-luasocket
Version: v3.0.rc1.211.gc89a931
Release: 1%{?dist}
Source0: https://github.com/diegonehab/luasocket/archive/c89a931cc31f219d51dd32a8a253d5ee3fbd31be.tar.gz
Summary: Lua sockets library
URL: https://github.com/diegonehab/luasocket
License: LuaSocket 3.0 license
Group: Development/Languages
Requires: lua53
BuildRequires: lua53

%description
Network socket support for Lua

%prep
%setup -q -n luasocket-c89a931cc31f219d51dd32a8a253d5ee3fbd31be

%build
make linux LUAV=5.3 LUAINC_linux=/opt/lua53/include LUAPREFIX_linux=/opt/lua53

%install
make install LUAV=5.3 LUAINC_linux=/opt/lua53/include LUAPREFIX_linux=/opt/lua53 INSTALL_TOP=%{buildroot}/opt/lua53

%files
/opt/lua53
